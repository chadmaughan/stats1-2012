# Statistics One, Lecture 3, example script
# Read data, plot histograms, get descriptives

# some of the functions used in this example
# are available in this 'pysch' library
library(psych)

# get help on functions
help(read.table)

# read in the sample wine data into a 'data.frame'
ratings<-read.table("/Users/chadmaughan/workspaces/coursera/statistics-one/r-intro/STATS1.EX.01.TXT",header=T)

# get the type of an object
class(ratings)

# returns names of the columns
names(ratings)

# display a data frame (opens new window)
View(ratings)

# view the beginning & last of the data frame
head(ratings)
tail(ratings)

# get help on 'hist' function
help(hist)

# plot a histogram
#  '$' picks out one of the columns in the data.frame
hist(ratings$RedTruck)

# plot four histograms on a single page
#  matrix() - creates matrix from values
#  c(1,2,3,4) - combines values into a vector or list
layout(matrix(c(1,2,3,4),2,2,byrow=TRUE))

# plot histograms
hist(ratings$WoopWoop,xlab="Rating")
hist(ratings$RedTruck,xlab="Rating")
hist(ratings$HobNob,xlab="Rating")
hist(ratings$FourPlay,xlab="Rating")

# density plot

# get the descriptive statistics for the variables in the data.frame
describe(ratings)
